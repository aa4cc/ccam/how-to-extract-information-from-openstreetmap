# How to extract data from an OpenStreetMap map

There two ways to get the data:

1. Getting a full map of a selected region directly from [OpenStreetMap.org](https://www.openstreetmap.org/). Find the region of interest with the appropriate zoom-in and then click `Export`. Even then you are still given an opportunity to manually select the region to be exported (by drawing a rectangle). If the region is not small enough, the system complains, but it recommends trying one of the alternative sources (mirrors) such as Overpass API (specialized just for reading, hence fast and unlimited data).
2. Extracing just some special features for a given region using [Overpass Turbo](https://overpass-turbo.eu/) web service. It expect that your requirement is formulated using a special scripting language. Fortunately, it also offers a helpful Wizard (in Czech: Průvodce) that allows formulating the requirement is a more human-like language. For example, the data for the rails for the tram number 7 in Ostrava can be be obtained by 
```
"route"="tram" and "ref"="7" in Ostrava
```
After running the generated script, it highlights the data selected for export in the map and then offers exporting the data in one of the formats GeoJSON, GPX, KML, OSM data. 

## Matlab (./matlab_package/)

A minimalistic package for extracting (parsing, visualization) the .osm files from OpenStreet Maps. The package is focused mainly on working with tram lines/tracks.

The package is based on the functions from:
https://www.mathworks.com/matlabcentral/fileexchange/35819-openstreetmap-functions

See **./matlab_package/README.md** for more information.


