function [members] = find_tram_network(name,relations)
%find_tram_network: Find given tram network in relations of osm_xml.
%   Return all members of network.
%   As name, enter for example: 'Tram 17: Levsk�ho -> Vozovna Kobylisy'
%
%   Iterates through all tags of all relations and finds name of tram
%   network. When network is found, return all its members. Otherwise empty
%   cell and display warning.
members = {};
for i=1:size(relations, 2)
    %     if(isfield(relations{i}, 'tag'))    % maybe redundant
    for j=1:size(relations{i}.tag,2)
        if size(relations{i}.tag,2) == 1
            % Because of relations which has only one tag.
            if (strcmp(relations{i}.tag.Attributes.v, name))
                members = relations{i}.member;
                return;
            end
        else
%             disp(relations{i}.tag{j}.Attributes.v);
            if (strcmp(relations{i}.tag{j}.Attributes.v, name))
                members = relations{i}.member;
                return;
            end
        end
    end
    %     end
end
warning('Network has not been found.');
end

