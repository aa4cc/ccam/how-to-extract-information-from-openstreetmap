function tram_railtrack_merged = merge_ways(tram_railtrack)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
% tram_railtrack_merged = cell(4,1);
nodes_id = [];
nodes_coords = [];
nodes_lines = [];
nodes_distances = [];
k = 1;
for i=1:size(tram_railtrack, 2)
    node_num = size(tram_railtrack{2,i},2);
    if (node_num > 0)
        if (~isempty(nodes_id))
            if (nodes_id(k) == tram_railtrack{2,i}(1,1))
                nodes_id = cat(2, nodes_id,tram_railtrack{2,i}(1,2:node_num));
                nodes_coords = cat(2, nodes_coords,tram_railtrack{3,i}(:,2:node_num));
            else
                nodes_id = cat(2, nodes_id,tram_railtrack{2,i}(1,1:node_num));
                nodes_coords = cat(2, nodes_coords,tram_railtrack{3,i}(:,1:node_num));
            end
        else
            nodes_id = cat(2, nodes_id,tram_railtrack{2,i}(1,1:node_num));
            nodes_coords = cat(2, nodes_coords,tram_railtrack{3,i}(:,1:node_num));
        end
        nodes_lines = cat(2, nodes_lines,tram_railtrack{4,i});
        nodes_distances = cat(2, nodes_distances,tram_railtrack{5,i});
        k = k + node_num - 1;
    end
end
tram_railtrack_merged.ids = nodes_id;
tram_railtrack_merged.coords = nodes_coords;
tram_railtrack_merged.lines = nodes_lines;
tram_railtrack_merged.distances = nodes_distances;
end

