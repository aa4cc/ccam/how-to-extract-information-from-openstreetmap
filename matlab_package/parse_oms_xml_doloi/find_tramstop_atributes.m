function names_and_locations = find_tramstop_name(tramstops_id, osm_xml)
%find_tramstop_name Find names from tram stops ID and osm xml.
%   Return cell of strings.
num_of_nodes = size(osm_xml.node, 2);
num_of_tramstops = size(tramstops_id, 2);
names_and_locations = cell(2,num_of_tramstops);
% locations = cell(1,num_of_tramstops);
for i=1:num_of_nodes
    for j=1:num_of_tramstops
        if(str2double(osm_xml.node{i}.Attributes.id) == tramstops_id{j})
            for k=1:size(osm_xml.node{i}.tag, 2)
                if strcmp(osm_xml.node{i}.tag{k}.Attributes.k, 'name')
                    names_and_locations{1,j} = osm_xml.node{i}.tag{k}.Attributes.v; % WARNING: Not generalized. Assuption of tag name on 1st position.
                    break;
                end
            end
            names_and_locations{2,j} = [str2double(osm_xml.node{i}.Attributes.lon); str2double(osm_xml.node{i}.Attributes.lat)];
        end
    end
end
end