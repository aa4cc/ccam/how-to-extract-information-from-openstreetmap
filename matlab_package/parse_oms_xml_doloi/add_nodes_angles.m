function [tram_track] = add_nodes_angles(tram_merge_track)
%add_nodes_angles Summary of this function goes here
%   Detailed explanation goes here

num_of_nodes = size(tram_merge_track.ids,2);
angles = zeros(1,num_of_nodes-1);

for i=2:num_of_nodes-1
    coord_1 = tram_merge_track.coords(:,i-1);
    coord_2 = tram_merge_track.coords(:,i);
    coord_3 = tram_merge_track.coords(:,i+1);
    altitudes = [tram_merge_track.altitude(i-1), tram_merge_track.altitude(i), tram_merge_track.altitude(i+1)];
    angles(i) = compute_diff_angle(coord_1, coord_2, coord_3, altitudes);
end

tram_track = tram_merge_track;
tram_track.angles = angles;

end

