function tram_track = osm_parse_custom_tramtrack(nodes_ids,parsed_osm)
%osm_parse_custom_tramtrack parse custom tram track using nodes id.
%   Detailed explanation goes here
nodes_ids = {nodes_ids};

nd_coords = find_nodes_coords(nodes_ids, parsed_osm);
size_of_track = size(nd_coords, 2);

tram_track.ids = nodes_ids{1};
tram_track.coords = nd_coords;

for i=1:size_of_track-1
    coord_1 = nd_coords(:,i);
    coord_2 = nd_coords(:,i+1);
    tram_track.lines(:,i) = create_interpol_line(coord_1, coord_2);
    tram_track.distances(i) = LLA_points_distance(coord_1,coord_2);
end

end

