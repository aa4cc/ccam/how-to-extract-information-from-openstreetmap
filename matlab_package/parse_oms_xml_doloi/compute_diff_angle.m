function [angle] = compute_diff_angle(c1,c2,c3, altitudes)
%compute_diff_angle compute change of angle (angle between two vectors)
%given by [c1;c2] and [c2;c3]

% Transform into cartesian ecef coordinates
% alt = 210; % FIXME: use real altitude
[x1, y1, z1] = lla2ecef_michael_kleder(deg2rad(c1(2)),deg2rad(c1(1)),altitudes(1));
[x2, y2, z2] = lla2ecef_michael_kleder(deg2rad(c2(2)),deg2rad(c2(1)),altitudes(2));
[x3, y3, z3] = lla2ecef_michael_kleder(deg2rad(c3(2)),deg2rad(c3(1)),altitudes(3));

A1 = [x1; y1; z1];
A2 = [x2; y2; z2];
A3 = [x3; y3; z3];

n = [x2,y2,z2] / norm([x2,y2,z2]);
v1 = A2 - A1;
v2 = A3 - A2;
% sin_angle = dot(cross(n, v1),v2);
sig_angle = dot(cross(n, v1),v2);
% angle = sign(sin_angle)*atan2(norm(cross(v1,v2)), dot(v1,v2));
angle = sign(sig_angle)*acos(dot(v1,v2)/(norm(v1)*norm(v2)));

% if abs(angle - angle2) > 1e-5
% disp(angle == angle2);
% disp(angle2);
% disp('');
% end

end

