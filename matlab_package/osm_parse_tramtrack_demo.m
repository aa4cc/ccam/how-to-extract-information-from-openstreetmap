clc;
clear;
% Adding all subfolder in current directory into path
MyPath = pwd;
MyDir = MyPath(1:strfind(MyPath,';')-1);
MyWorkDir = genpath(MyPath);
addpath(MyWorkDir, '-end');
clear;
clc;
%%
% This demo shows how to extract tram track from OSM file and find particular
% tram track based on its name

%% 1) Download the .osm file
% - Small areas (~ 3km x 3km): use https://www.openstreetmap.org/
%   - Select 'small' area and click 'Export'
% - Large areas (whole cities): you can use https://export.hotosm.org/en/v3/exports/new/describe
% - Another option (not tested): https://wiki.openstreetmap.org/wiki/Planet.osm

% Auxilary tools
%   - Osmosis: https://wiki.openstreetmap.org/wiki/Osmosis
%       - Use case: keeping only tram-related things in the .osm file. Post-processing is then much faster
%         - E.g.:
%           --read-xml INPUT_FILE.osm --way-key-value keyValueList="railway.tram,railway.tram_stop" --used-node --write-xml OUTPUT_FILE.osm
%   - https://wiki.openstreetmap.org/wiki/Osmconvert
%       - Convert data to .osm
%   - https://wiki.openstreetmap.org/wiki/Osmfilter#Tags_Filter
%       - Use cases:
%           - Filtering out things that are not needed for transporation
%           - merging more files.

%% Try to parse some map:
openstreetmap_filename = './KN-VN-NR.osm';

% Note that if you download original .osm file, the tag value of the
% tramtrack name IS DIFFERENT. There are different symbols but matlab has a
% problem with storing e.g. Czech symbols. So try to first find the
% tramtrack by looking to xml file. It should be something like:
% <tag k="name" v="Tram 6: ....

% The tram_name was renamed in .osm file
tram_name = 'Tram 6: Kub�nsk� n�mest� -> Palmovka';

% convert XML -> MATLAB struct. This might take some time. For this file,
% it takes around 20 seconds.
[parsed_osm, osm_xml] = parse_openstreetmap(openstreetmap_filename);

%% Parsing the tram track

% Parse elevation map
elevation_map_available = 1; % NOTE: set 0 or 1 to indicate if elevation map is available
if elevation_map_available == 1
    disp("Elevation map is available, parsing the data");
    elevation_map_path = './dem_KN-VN-NR.tif'; % should be in .tif format
    elev_data = double(importdata(elevation_map_path));

    % The constants to align two maps (OSM and elevation map).
    offset = 1e-3*[0.8100, 1.100]; % To correct errors in map alignment
    north = 50.09257905441916;
    south = 50.07230200470836;
    west = 14.40499486110664;
    east = 14.452676093081509;
    limits = [north, south, west, east];
    altitude_struct = parse_elev_data(elev_data, limits, offset);
else
    disp("Elevation map is not available, constant altitude is considered");
    altitude_struct.x = 1:10;
    altitude_struct.y = 1:10;
    altitude_struct.data = 250*ones(10,10);
end

% Note that you have only those nodes of the tram track which are in the .osm file
[tram_track, tramstops] = parse_tram_network(tram_name,osm_xml, parsed_osm, altitude_struct);

%% Visualization
fig = figure;
ax = axes('Parent', fig);
hold(ax, 'on');
pcolor(altitude_struct.x,altitude_struct.y,altitude_struct.data);
shading interp;
plot_way(ax, parsed_osm);
plot_tramstops_in_map(tramstops);



